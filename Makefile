CC="g++-8"

vm:
	$(CC) ./src/vm.cpp -o ./bin/vm.elf -std=c++17

vm_debug:
	$(CC) -g ./src/vm.cpp -o ./debug/vm.debug -std=c++17

asm:
	$(CC) ./src/asm.cpp -o ./bin/asm.elf -std=c++17

test:
	$(CC) ./tests/test.cpp -o ./tests/test.elf -std=c++17
	./tests/test.elf

bc:
	./bin/asm.elf ./vmtests/asmtest.basm ./vmtests/asmout.ef
	./bin/vm.elf ./vmtests/asmout.ef

be_core:
	$(CC) ./betterfly-core/*.cpp -o ./bin/betterfly-core-compiler.elf -std=c++17

all:
	vm
	asm