#include <iostream>
#include <fstream>
#include <array>
#include <vector>
#include <map>

using namespace std;

template<typename T> 
constexpr std::ostream& write(std::ostream& os, const T& value) { 
    return os.write(reinterpret_cast<const char*>(&value), sizeof(T)); 
} 
  
template<typename T> 
constexpr std::istream & read(std::istream& is, T& value) { 
    return is.read(reinterpret_cast<char*>(&value), sizeof(T)); 
} 

unsigned long long fsize(const std::string &fileName){
    ifstream file(fileName.c_str(), ios::binary);
    file.seekg(0, ios::end);
    unsigned long long fileSize = file.tellg();
    file.close();
    return fileSize;
}

constexpr bool isalphanum(char c){
    return ( c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' || c <= '9' || c == '_' );
    //return ( c >= 33 && c <= 125);
}

using VMtype = double;

map<string, VMtype>rules = 
{
    { "re1", 0x1 },
    { "re2", 0x2 },
    { "re3", 0x3 },
    { "re4", 0x4 },
    { "nop", 0x0 },
    { "add", 0x1 },
    { "sbt", 0x2 },
    { "mtp", 0x3 },
    { "div", 0x4 },
    { "opp", 0x5 },
    { "prt", 0x6 },
    { "push", 0x7 },
    { "addd", 0x8 },
    { "sbtd", 0x9 },
    { "mtpd", 0xA },
    { "divd", 0xB },
    { "oppd", 0xC },
    { "pop", 0xD },
    { "prtc", 0xE },
    { "mov", 0x11 },
    { "inp", 0x12 },
    { "inpc", 0x13 },
    { "inf", 0x14 },
    { "sup", 0x15 },
    { "equl", 0x16 },
    { "inf_eq", 0x17 },
    { "sup_eq", 0x18 },
    { "diff", 0x19 },
    { "not", 0x20 },
    { "pass", 0x21 },
    { "jmp", 0x22 },
    { "lbl", 0x23}
};

int main(int argc, char**argv){
    if(argc == 3){
        ifstream file(argv[1]);
        char curcar;
        array<string, 4> list;
        vector<array<string, 4> > sequenced;
        string curstr = "";
        size_t curindex = 1;
        unsigned long long count = 0;
        bool in = false;
        /*while(file.get(curcar)){
            if( curcar >= 33 && curcar <= 126 ){
                if(in){
                    curstr = curstr + curcar;
                } else{
                    curstr = curcar;
                    in = true;
                    curindex++;
                }
            }else{
                if(curindex != 0){
                    if(in){
                        //for(int i = 0; i < curindex%4; i++){
                        list[curindex%4] = curstr;           ///////////because of the array in the vector : to implement
                        //}
                        if(curindex%4 == 0){
                            sequenced.push_back(list);
                        }
                        //sequenced.push_back(list);
                        in = false;
                    }
                }
            }
            count++;
        }*/
        if(file){
            while(file >> curstr){
                if(curstr == "re1") curstr = "1";
                if(curstr == "re2") curstr = "2";
                if(curstr == "re3") curstr = "3";
                if(curstr == "re4") curstr = "4";
                list[(curindex-1)%4] = curstr;
                if(curindex%4 == 0 && curindex > 0){
                    sequenced.push_back(list);
                }
                curindex++;
            }
        }
        file.close();
        //file sequenced
        ofstream byteCodeFile(argv[2], ios::binary);
        vector<VMtype> byteCode;
        bool hasChanged = false; //unsigned long long int count = 0;

        for(auto& curline : sequenced){
            hasChanged = true;
            for(auto& cursym : curline){
                auto search = rules.find(cursym);
                //if(count%4 == 0){
                if(hasChanged){
                    if(search == rules.end()){
                        /*byteCode.push_back(stoll(cursym));
                        write(byteCodeFile, stoll(cursym));*/
                        cerr << "ERROR, INSTRUCTION NOT FOUND" << endl;
                        return 1;
                    } else{
                        byteCode.push_back(rules[cursym]);
                        write(byteCodeFile, rules[cursym]);
                    }
                } else{
                    byteCode.push_back(stod(cursym));
                    write(byteCodeFile, stod(cursym));
                }
                count++;
                hasChanged = false;
            }
            
        }
        
        /*for(auto& scrut : byteCode){
            cout << scrut << "\t";
        }*/

    }
}