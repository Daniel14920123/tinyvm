#include <iostream>
#include <stack>
#include <fstream>
#include <vector>
#include <array>
#include <unordered_map>
#include "../include_gen/macros.hpp"

using namespace std;

template<typename T> 
constexpr std::ostream& write(std::ostream& os, const T& value) { 
    return os.write(reinterpret_cast<const char*>(&value), sizeof(T)); 
} 
  
template<typename T> 
constexpr std::istream & read(std::istream& is, T& value) { 
    return is.read(reinterpret_cast<char*>(&value), sizeof(T)); 
} 

unsigned long long fsize(const std::string &fileName){
    ifstream file(fileName.c_str(), ios::binary);
    file.seekg(0, ios::end);
    unsigned long long fileSize = file.tellg();
    file.close();
    return fileSize;
}



#define OPERANDDEST(op, bas1, bas2, dest)  { long long za = bas1.top(); bas1.pop(); long long zb = bas2.top(); bas2.pop(); dest.push( za op zb ); }
#define OPERANDNDEST(op, bas1, bas2, dest) { long long za = bas1.top(); bas1.pop(); bas1.push(za); long long zb = bas2.top(); dest.push(za op zb); }


#define HEADER_SIZE 0

#define OPLENGTH 4

#define R1   0x1
#define R2   0x2
#define R3   0x3
#define R4   0x4

#define NOP  0x0    //nop                                           /
#define ADD  0x1    //non-destructive addition                      /
#define SBT  0x2    //non-destructive substraction                  /
#define MTP  0x3    //non-destructive multiplication                /
#define DIV  0x4    //non-destructive division                      /
#define OPP  0x5    //non-destructive mumtiplication by -1          /
#define PRT  0x6    //prints the number                             /
#define PUSH 0x7    //push a value in a register                    /
#define ADDD 0x8    //destructive addition                          /
#define SBTD 0x9    //destructive substraction                      /
#define MTPD 0xA    //destructive multiplication                    /
#define DIVD 0xB    //destructive division                          /
#define OPPD 0xC    //destructive multiplication by -1              /
#define POP  0xD    //pops a value                                  /
#define PRTC 0xE    //prints as a character                         /
#define MOV  0x11   //moves the top of a register on another        /
#define INPI 0x12   //store a value given by user in a register     /
#define INPC 0x13   //store a char given by user in a register      /
#define INF  0x14   //test                                          /
#define SUP  0x15   //test                                          /
#define EQU  0x16   //test                                          /
#define INFE 0x17   //test                                          /
#define SUPE 0x18   //test                                          /
#define DIFF 0x19   //test                                          /
#define NOT  0x20   //operator not                                  /
#define PASS 0x21   //goes on the ARG1 operation further ( + / - )  /
#define JMP  0x22
#define LABL 0x23
#define ADDV 0x24
#define SBTV 0x25
#define MTPV 0x26
#define DIVV 0x27

using VMtype = double;

#define ARG1 (scrut1[1])
#define ARG2 (scrut1[2])
#define ARG3 (scrut1[3])

#define register(x) (stacks[x-1])

__uint8_t getNArgs(const std::array<VMtype, OPLENGTH> toGet){
    __uint8_t toReturn = OPLENGTH;
    for(__uint8_t scrut = OPLENGTH - 1; scrut >= 0; scrut--){
        if(toGet[scrut] != NOP){
            return scrut + 1;
        }
    }
}

int main(int argc, char**argv){ 
    std::array<stack<VMtype>, 4> stacks;
    if(argc == 2){
        std::ifstream file(argv[1], ios::binary);
        std::array<VMtype, OPLENGTH> rawOP;
        std::vector< std::array<VMtype, OPLENGTH> > OPS;
        VMtype op;
        unsigned long long int gone = 0;
        unsigned long long size = fsize(argv[1]);

        for(gone = 0; gone < size/sizeof(double); gone++){
            read(file, op);
            if(gone >= HEADER_SIZE){
                if(gone % OPLENGTH == 0){
                    if(gone >= HEADER_SIZE + 3){
                        OPS.emplace_back(rawOP);
                    }
                    rawOP[0] = op;
                }
                else{
                    
                    rawOP[gone % OPLENGTH] = op;
                }
            }
        }
        OPS.push_back(rawOP);
        
        size_t count = 0;
        std::unordered_map<size_t, long long> labels;
        for(auto&& scrut1 : OPS){
            if(scrut1[0] == LABL ){
                if(getNArgs(scrut1) == 2){
                    labels.emplace(ARG1, count);
                } else {
                    cerr << "Error on operation N " << count + 1 << endl;
                    return -1; 
                }
            }
            count++;
        }


        bool done;
        size_t fallback = 0;
        size_t opcounter = 0;
        //qlogv(std::cout, OPS.size());
        for( opcounter = 0; opcounter < OPS.size(); opcounter++){
            auto& scrut1 = OPS[opcounter];
            done = true;
            switch(getNArgs(scrut1)){
                case 1:
                    //{qlogv(std::cout, static_cast<long long>(scrut1[0]));
                    switch(static_cast<long long>(scrut1[0])){
                        case NOP:
                            break;
                        case PASS:
                            opcounter++;
                            break;
                        default:
                            done = false;
                            break;
                    }//}
                case 2:
                    //{qlogv(std::cout, static_cast<long long>(scrut1[0]));
                    switch(static_cast<long long>(scrut1[0])){
                        case JMP:
                            fallback = opcounter + 1;
                            opcounter = labels[ARG1];
                            break;
                        case PRT:
                            cout << register(ARG1).top();
                            break;
                        case PRTC:
                            cout << static_cast<char>(register(ARG1).top());
                            break;
                        case OPP:
                            register(ARG1).push(register(ARG1).top() * -1);
                            break;
                        case OPPD:
                            register(ARG1).top() *= -1;
                            break;
                        case POP:
                            register(ARG1).pop();
                            break;
                        case INPI:
                            {
                                double tempint;
                                cin >> tempint;
                                register(ARG1).push(tempint);
                            }
                            break;
                        case INPC:
                            {
                                char temp;
                                cin.get(temp);
                                register(ARG1).push(static_cast<int>(temp));
                            }
                            break;
                        case PASS:
                            opcounter += ARG1;
                            break;
                        case LABL:
                            break;
                        /*case ADDD:
                            OPERANDDEST(+, register(ARG1), register(ARG1), register(ARG1));
                        case SBTD:
                            OPERANDDEST(-, register(ARG1), register(ARG1), register(ARG1));
                        case MTPD:
                            OPERANDDEST(*, register(ARG1), register(ARG1), register(ARG1));
                        case DIVD:
                            OPERANDDEST(/, register(ARG1), register(ARG1), register(ARG1));
                        case ADD:
                            OPERANDNDEST(+, register(ARG1), register(ARG1), register(ARG1));
                        case SBT:
                            OPERANDNDEST(-, register(ARG1), register(ARG1), register(ARG1));
                        case MTP:
                            OPERANDNDEST(*, register(ARG1), register(ARG1), register(ARG1));
                        case DIV:
                            OPERANDNDEST(/, register(ARG1), register(ARG1), register(ARG1));*/
                        default:
                            done = false;  
                            break;                      
                    }//}
                case 3:
                    //{qlogv(std::cout, static_cast<long long>(scrut1[0]));
                    switch(static_cast<long long>(scrut1[0])){
                        case JMP:
                            if(register(ARG2).top()!=0){
                                fallback = opcounter + 1;
                                opcounter = labels[ARG1];
                            }
                            break;
                        case PASS:
                            if(register(ARG2).top()!=0){
                                opcounter+= ARG1;
                            }
                            break;
                        case PUSH:
                            register(ARG1).push(ARG2);
                            //cout << "check" << endl;
                            break;
                        case MOV:
                            register(ARG2).push(register(ARG1).top());
                            register(ARG1).pop();
                            break;
                        case NOT:
                            register(ARG1).push(!register(ARG2).top());
                            break;
                        default:
                            done = false;
                            break;
                    }//}
                case 4:
                    //{qlogv(std::cout, static_cast<long long>(scrut1[0]));
                    switch(static_cast<long long>(scrut1[0])){
                        case ADDD:
                            OPERANDDEST(+, register(ARG1), register(ARG2), register(ARG3));
                            break;
                        case SBTD:
                            OPERANDDEST(-, register(ARG1), register(ARG2), register(ARG3));
                            break;                     
                        case MTPD:
                            OPERANDDEST(*, register(ARG1), register(ARG2), register(ARG3));
                            break;
                        case DIVD:
                            OPERANDDEST(/, register(ARG1), register(ARG2), register(ARG3));
                            break;
                        case ADD:
                            OPERANDNDEST(+, register(ARG1), register(ARG2), register(ARG3));
                            break;
                        case SBT:
                            OPERANDNDEST(-, register(ARG1), register(ARG2), register(ARG3));
                            break;
                        case MTP:
                            OPERANDNDEST(*, register(ARG1), register(ARG2), register(ARG3));
                            break;
                        case DIV:
                            OPERANDNDEST(/, register(ARG1), register(ARG2), register(ARG3));
                            break;
                        case EQU:
                            register(ARG1).push(register(ARG2).top() == register(ARG3).top());
                            break;
                        case INF:
                            register(ARG1).push(register(ARG2).top() < register(ARG3).top());
                            break;
                        case SUP:
                            register(ARG1).push(register(ARG2).top() > register(ARG3).top());
                            break;
                        case INFE:
                            register(ARG1).push(register(ARG2).top() <= register(ARG3).top());
                            break;
                        case SUPE:
                            register(ARG1).push(register(ARG2).top() >= register(ARG3).top());
                            break;
                        case DIFF:
                            register(ARG1).push(register(ARG2).top() != register(ARG3).top());
                            break;
                        default:
                            done = false;
                            break;
                    }//}
                
            }
            /*if(!done){
                cerr << "Error on operation N " << opcounter + 1 << endl;
                return opcounter + 1;
            }*/
            
        }
    }
}
